<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Parsers\ShoptetXMLOrders;
use App\Connectors\GoDeliverApi;
use Nette;

final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @var ShoptetXMLOrders @inject */
    public $shoptetXMLOrders;

    /** @var GoDeliverApi @inject */
    public $goDeliverApi;

    protected $xmlOrders = null;

    public function renderDefault()
    {
        $this->template->title = "Integrace GoDeliver pro DobrotySPribehem.cz";
        $this->template->orders = $this->xmlOrders;
    }

    public function handleLoadOrders()
    {
        if (file_exists("temp_orders")) {
            $xmlOrders = file_get_contents('temp_orders');
        } else {
            $xmlOrders = file_get_contents(ShoptetXMLOrders::FEED_URL);
            file_put_contents("temp_orders", $xmlOrders);
        }

        $this->xmlOrders = $this->shoptetXMLOrders->parseOrdersForGoDeliver($xmlOrders);
    }

    public function handlePushOrdersToGoDeliver() {
        $xmlOrders = file_get_contents('temp_orders');
        $this->xmlOrders = $this->shoptetXMLOrders->parseOrdersForGoDeliver($xmlOrders);

        foreach ($this->xmlOrders as $order) {
            $this->goDeliverApi->pushOrder($order);
        }

        $this->flashMessage("Objednávky byly propsány do GoDeliver");
    }

}
