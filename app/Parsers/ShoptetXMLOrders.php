<?php

namespace App\Parsers;

use DateTime;

class ShoptetXMLOrders
{
    const GODELIVER_CLASS = "SHIPPING74";
    const GODELIVER_CLASS2 = "SHIPPING83";
    const FEED_URL = "https://www.dobrotyspribehem.cz/export/orders.xml?patternId=-11&hash=50183dc80b7de1a114c3effd04d75dab868bbac026b392bcf331fe99d290f4e0";

    private function nextDeliveryDay()
    {
        $date = new DateTime();
        $date->modify('tomorrow');

        return $date->format('Y-m-d');
    }

    public function parseOrdersForGoDeliver($xmlContent)
    {
        $filteredOrders = [];

        $orders = simplexml_load_string($xmlContent);
        // apply filter
        foreach ($orders as $order) {
            foreach ($order->ORDER_ITEMS as $no => $data) {
                foreach ($data->ITEM as $items) {
                    if ($items->TYPE == "shipping" &&
                        ($items->CODE == self::GODELIVER_CLASS
                            or
                            $items->CODE == self::GODELIVER_CLASS2)
                        &&
                        ($order->STATUS == "Zkompletováno"
                            or
                            $order->STATUS == "Právě vyráběno")
                    ) {
                        $filteredOrders[] = $order;
                    }
                }
            }
        }

        $createdOrders = [];
        //iterate orders
        foreach ($filteredOrders as $order) {
            $pickupTime = new \DateTime($this->nextDeliveryDay() . " 08:00");
            $deliveryTime = new \DateTime($this->nextDeliveryDay() . " 12:00");

            $oid = (string)$order->ORDER_ID;

            $orderRequest = [
                'business_id' => null,
                "note" => (string)$order->SHOP_REMARK ?? null,
                "delivery" => [
                    "pickup_address" => [
                        "street_name" => "Kiliánská",
                        "number" => "367",
                        "postal_code" => "252 06",
                        "city" => "Davle",
                        "address_note" => "Portus Praha - Dobroty s Pribehem.",
                    ],
                    "pickup_contact" => [
                        "first_name" => "Daniel",
                        "last_name" => "Pastva",
                        "phone_number" => 420725849883,
                        "mail" => "pastva@dobrotyspribehem.cz"
                    ],
                    "destination_address" => [
                        "street_name" => (string)$order->CUSTOMER->SHIPPING_ADDRESS->STREET,
                        "number" => $this->decideHouseNumber((string)$order->CUSTOMER->SHIPPING_ADDRESS->STREET),
                        "postal_code" => (string)$order->CUSTOMER->SHIPPING_ADDRESS->ZIP,
                        "city" => (string)$order->CUSTOMER->SHIPPING_ADDRESS->CITY,
                        "address_note" => (string)$order->CUSTOMER->SHIPPING_ADDRESS->NAME ?? 'Neuvedeno',
                    ],
                    "destination_contact" => [
                        "first_name" => (string)$order->CUSTOMER->SHIPPING_ADDRESS->NAME,
                        "last_name" => (string)$order->CUSTOMER->SHIPPING_ADDRESS->NAME,
                        "phone_number" => trim(str_replace('+', null, $order->CUSTOMER->PHONE)) ?? 0,
                        "mail" => (string)$order->CUSTOMER->SHIPPING_ADDRESS->MAIL
                    ],
                    "packages" => [
                        [
                            "description" => "Objednavka - #{$oid}",
                            "category" => "FOOD",
                            "size_type" => "SMALL",
                            "weight" => 1000
                        ]
                    ],
                    "delivery_time" => [
                        "pickup_time_block" => [
                            "from_time" => $pickupTime->getTimestamp(),
                            "to_time" => ($pickupTime->getTimestamp() + 3600)
                        ],
                        "drop_time_block" => [
                            "from_time" => $deliveryTime->getTimestamp(),
                            "asap" => true,
                        ]
                    ]
                ]
            ];

            $createdOrders[] = $orderRequest;
        }

        return $createdOrders;
    }

    private function decideHouseNumber($rawStreet)
    {
        $data = explode(" ", $rawStreet);
        $last = count($data) - 1;

        $streetNo = $data[$last] ?? null;

        return $streetNo ?? 1;
    }
}
