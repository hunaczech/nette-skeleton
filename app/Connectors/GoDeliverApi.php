<?php

namespace App\Connectors;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use KuryrBox\Entities\OrderEntity;
use KuryrBox\Repositories\OrderRepository;
use Tracy\ILogger;

class GoDeliverApi
{
    /** @var ILogger */
    protected $logger;

    /** @var Client */
    protected $client;

    /** @var string */
    private $partnerId;

    const BASE_API = "https://api.godeliver.co/delivery";

    /**
     * GoDeliverApi constructor.
     * @param ILogger $logger
     */
    public function __construct(ILogger $logger)
    {
        $this->partnerId = "portus";
        $this->client = new Client();
        $this->logger = $logger;
    }

    public function pushOrder(array $orderRequest)
    {
        $endpoint = self::BASE_API . "/order";

        $orderRequest['business_id'] = $this->partnerId;

        $this->request = $this->response = null;

        $this->request = [
            'json' => $orderRequest
        ];

        try {
            $this->response = $this->client->request("POST", $endpoint, $this->request);
        } catch (ClientException | ServerException $exception) {
            $this->logger->log(
                'Failed to call GoDeliver API - order detail: ' . $exception->getMessage(),
                ILogger::EXCEPTION
            );

            $this->logger->log(
                var_export($this->request),
                ILogger::INFO
            );

            $this->response = [
                'status' => "failed",
                'error' => $exception->getMessage(),
            ];

            return null;
        }

        $this->response = $this->response->getBody()->getContents();

        return json_decode($this->response) ?? null;
    }
}
